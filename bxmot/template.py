sys.path.append('/usr/local/encore')

from encoreio import *

# This can only run from the front-end-computer, so you can develop on the VM and save it on the NFS. (Anywhere under /user/myname/)
#
# Opens the device files. For example:
#
#   Module.get_instance( Foobar, 123 )
#
# Will open the file /dev/Foobar.123. The device files are numbered from 0, so a real call looks more like:
#
#   Module.get_instance( bxmydevice, 0 )

my_device = Module.get_instance("bxmotor", 0)

def main():

    # Returns a tuple where the item at index 0 is the register content
    ada_register = my_device.read("ADAMYREGISTER", dma=1, time=0)

    # Explains itself
    my_device.write("ADAMYREGISTER", 0x1234)

if __name__ == '__main__':
    main()


#! /usr/bin/env python3

import argparse
import os
from datetime import datetime
import sys

sys.path.append('/usr/local/encore')

from time import sleep
from encoreio import *

bxmotor = Module.get_instance("bxmotor", 0)

DIA_APPROACH_START_MAX = 20
DIA_MOTOR_CHANNEL = 2
DIA_MOTOR_COEF = 4.36
DIA_MOTOR_OFFSET = 0
DIA_MOTOR_MIN = 0
DIA_MOTOR_MAX = 20
DIA_MOTOR_STEPS_MAX = 50
DIA_WRITE_FREQ = 1
DIA_POS_ARRAY_MAX_POINTS = 40

positionArray = []

def main():

    parser = argparse.ArgumentParser(description='BXMOT Test')

    group = parser.add_mutually_exclusive_group(required=True)

    group.add_argument('--step-to', nargs=3, metavar=('start', 'stop', 'step'), type=float)
    group.add_argument('--single-step', nargs=2, metavar=('start', 'step'), type=float)
    group.add_argument('--stability', nargs=1, metavar=('start'), type=float)

    args = parser.parse_args()

    if getattr(args, 'step_to'):
        start, stop, step = getattr(args, 'step_to')

        return do_step_range_test(start, stop, step, DIA_WRITE_FREQ)

    if getattr(args, 'single_step'):
        start, step = getattr(args, 'single_step')

        return do_single_step_test(start, step)

    if getattr(args, 'stability'):
        start, = getattr(args, 'stability')

        return do_stability_test(start, DIA_WRITE_FREQ)
    else:
        print(getPosition())

def getName():
    name = ""

    for i in range(0x20, 0x29):
        bxmotor.write("FHWRITE", (i,))
        name += chr(bxmotor.read("FHREAD", dma=1, time=0)[0] & 0x00FF)

    return name


def motorOn():
    reg = bxmotor.read("ADAPADR", dma=1, time=0)[0]

    reg = reg & (0x1f - 4)

    bxmotor.write("ADAPADR", (reg,))
    sleep(0.01)


def motorOff():
    reg = bxmotor.read("ADAPADR", dma=1, time=0)[0]

    reg = reg | (0x01 << DIA_MOTOR_CHANNEL)

    bxmotor.write("ADAPADR", (reg,))
    sleep(0.01)


def getPosition():
    bxmotor.write("ADAADRCRAM0", (0x200a,))
    bxmotor.write("ADAADCCONF", (0x0002,))
    bxmotor.write("ADAADCCONF", (0x0001,))

    reg = bxmotor.read("ADAADCFIFO", dma=1, time=0)[0] & 0x0FFF

    reg = (((20.00 * reg + 20.0) / 4096.00) - 10.000)

    return reg * DIA_MOTOR_COEF

def printStatus():
    status = bxmotor.read("MPXADR3", dma=1, time=0)[0]

    print()
    print('       ================')
    # print('        Position:     {}'.format(getPosition()))
    print('         Enabled:     {}'.format("1" if 0x0010 & status else "0"))
    print('   Pulse Enabled:     {}'.format("1" if isPulseEnabled() else "0"))
    print()
    print('    Plus current:     {}'.format("1" if 0x0001 & status else "0"))
    print('   Minus current:     {}'.format("1" if 0x0002 & status else "0"))
    print('      Plus volts:     {}'.format("1" if 0x0004 & status else "0"))
    print('     Minus volts:     {}'.format("1" if 0x0008 & status else "0"))
    print('        Time out:     {}'.format("1" if 0x0020 & status else "0"))
    print('          Manual:     {}'.format("1" if 0x0040 & status else "0"))
    print('      Move error:     {}'.format("1" if 0x0080 & status else "0"))
    print('       Limit neg:     {}'.format("1" if 0x0100 & status else "0"))
    print('       Limit pos:     {}'.format("1" if 0x0200 & status else "0"))
    print('       ================')

def print_position_array_size(sig, frame):
    print(len(positionArray))

def motor_to(target):

    print('motor_to: target = {}'.format(target))

    if target < DIA_MOTOR_MIN or target > DIA_MOTOR_MAX:
        print('{} is out of range', target)
        return

    voltage = -1 * target / DIA_MOTOR_COEF

    value = int(((voltage + 5.) * 0x1000) / 10)
    value = value & 0x0FFF
    value = value << 2

    bxmotor.write("ADADAC3", (value,))
    bxmotor.write("ADADACOUT", (2,))

def write_array_to_file(fname):
    try:
        os.remove(fname)
    except:
        pass

    fp = open(fname, 'w')

    for i, v in enumerate(positionArray):
        fp.write('{} {}\n'.format(i, v))

    fp.close()

def read_positions_from_bxmot(read_freq):
    while len(positionArray) < DIA_POS_ARRAY_MAX_POINTS:
        positionArray.append(getPosition())
        sleep(1 / read_freq)
        print(len(positionArray))


def approach_start_val(target):

    print('approach_start_val.begin: target = {}'.format(target))

    i = 0

    last_positions = []

    while i < DIA_APPROACH_START_MAX:
        i += 1

        motorOn()
        sleep(1)
        motor_to(target)
        sleep(2)
        motorOff()

        current_position = getPosition()

        print('approach_start_val.loop: pos = {}'.format(current_position))

        if abs(current_position - target) < 0.1:
            return current_position

        if last_positions == 5 and last_positions.count(current_position) == len(last_positions):
            print('approach_start_val.stopped_moving: target = {}, pos = {}'.format(target, current_position))
            break

        if len(last_positions) == 5:
            last_positions = last_positions[1:]

        last_positions.append(current_position)

def do_stability_test(target, read_freq):

    global positionArray

    positionArray = []

    approach_start_val(target)

    sleep(3)
    motorOff()
    read_positions_from_bxmot(read_freq)

    write_array_to_file('positions-stability-{}.txt'.format(datetime.today().strftime('%H:%M:%S')))

def enablePulse():
    sleep(0.01)
    sleep(1)

    reg = 0

    bxmotor.write("ADAPBDR", (reg,))

    print(bxmotor.read("ADACR0", dma=1, time=0))
    bxmotor.write("ADACR0", (0,))
    print(bxmotor.read("ADACR0", dma=1, time=0))

    # (Change Timer 2 Time Constant LSB Register)
    bxmotor.write("ADACR0", (0x19,))
    bxmotor.write("ADACR0", (0x00,))

    bxmotor.write("ADACR0", (0x18,))
    bxmotor.write("ADACR0", (0x00,))

    bxmotor.write("ADACR0", (0x0b,))
    bxmotor.write("ADACR0", (0x26,))


def disablePulse():
    sleep(0.01)
    reg = (DIA_MOTOR_CHANNEL + 1) << 5

    bxmotor.write("ADAPBDR", (reg,))
    bxmotor.write("ADACR0", (0,))

    # (Change Timer 2 Time Constant LSB Register)
    bxmotor.write("ADACR0", (0x19,))

    bxmotor.write("ADACR0", (0x01,))

    bxmotor.write("ADACR0", (0x18,))
    bxmotor.write("ADACR0", (0x00,))

    bxmotor.write("ADACR0", (0x0b,))
    bxmotor.write("ADACR0", (0x26,))


PULSE_TABLE = [
    [4, 50, ],
    [0.29999999999999999, 20, ], [0.20000000000000001, 15, ], [0.10000000000000001, 10, ], [0.074999999999999997, 8, ],
    [0.050000000000000003, 8, ], [0.01, 5, ], [0.001, 5, ], [0.001, 5, ], [0.001, 5, ]
]

def doPulse():
    ms = 10
    mslw = ms & 0xff
    mshw = int(ms / 0xff)

    reg = (DIA_MOTOR_CHANNEL + 1) << 5

    bxmotor.write("ADAPBDR", (reg,))
    bxmotor.write("ADACR0", (0,))

    bxmotor.write("ADACR0", (0x19,))

    bxmotor.write("ADACR0", (mslw,))

    bxmotor.write("ADACR0", (0x18,))
    bxmotor.write("ADACR0", (mshw,))

    bxmotor.write("ADACR0", (0x0b,))
    bxmotor.write("ADACR0", (0x26,))

def do_single_step_test(start, step):
    global positionArray
    positionArray = []

    approach_start_val(start)

    motorOn()
    sleep(3)

    start = getPosition()
    positionArray.append(start)


    target = start + step
    print('single step test: start = {:.1f}, target = {}, step = {:.1f}'.format(start, start + step, step))

    motor_to(target)

    sleep(1)

    last = getPosition()

    positionArray.append(last)

    write_array_to_file('positions-single-step-{}.txt'.format(datetime.today().strftime('%H:%M:%S')))

def isPulseEnabled():

    reg = bxmotor.read("ADAPBDR", dma=1, time=0)[0]

    reg = reg & 0x00E0
    reg = reg >> 5

    return reg == 4

def do_step_range_test(start, stop, step, write_freq):

    printStatus()
    motorOff()
    sleep(2)


    global positionArray
    positionArray = []

    approach_start_val(start)

    start = getPosition()

    print('step_range_test.start_reached: start = {:.1f}, stop = {:.1f}, step = {:.1f}, max_steps = {:.1f}'.format(start, stop, step, DIA_MOTOR_STEPS_MAX))

    i = 0

    motorOn()
    sleep(3)

    last_position = -1
    within_threshold = False
    gone_past = False
    stopped_moving = False
    last_positions = []

    nudge = 0
    using_nudge = False
    disable_section = False
    nudge_with_dir = 0

    while i < DIA_MOTOR_STEPS_MAX:

        i += 1
        pos = getPosition()
        positionArray.append(pos)

        if abs(pos - stop) < step:
            print("in threshold - current = {}, end = {}, step = {}".format(pos, stop, step))
            within_threshold = True

        if i > 1:
            gone_past = (last_position < stop and pos > stop) or (last_position > stop and pos < stop)

        last_position = pos

        if within_threshold:
            if step == 0.5:
                step = 0.2
            elif step == 0.2:
                step = 0.1
            elif step == 0.1:
                print('BREAK 1')
                break
            else:
                step = 0.5
            within_threshold = False
            continue

        if gone_past:
            print("gone past - current = {}, end = {}, step = {}".format(pos, stop, step))
            if step == 0.5:
                step = 0.2
            elif step == 0.2:
                step = 0.1
            elif step == 0.1:
                print('BREAK 1')
                break
            else:
                step = 0.5
            gone_past = False
            continue

        if not disable_section:
            if len(last_positions) < 2:
                last_positions.append(pos)
            else:
                all_values_same = True
                for prev_position in last_positions:
                    if prev_position != pos:
                        all_values_same = False
                        break

                if all_values_same:
                    stopped_moving = True
                    if using_nudge:
                        print('all values same')
                        break

                    using_nudge = True
                    nudge = 0.05
                    last_positions = []
                    disable_section = True
                    print('use nudge')

            last_positions = last_positions[1:] + [pos]

        sleep(1 / write_freq)

        step_with_dir = step if pos < stop else -step

        if using_nudge:
            motor_to(10)
            using_nudge = False
            nudge_with_dir += nudge if pos < stop else -nudge

        to = pos + step_with_dir + nudge_with_dir
        print("current = {}, motorTo = {}, nudge = {}".format(pos, to, nudge_with_dir))
        motor_to(to)

    write_array_to_file('positions-range-test-{}.txt'.format(datetime.today().strftime('%H:%M:%S')))


if __name__ == '__main__':
    main()

